A Time-Constrained Capacitated Vehicle Routing Problem in Urban E-Commerce Delivery
------------------

This project develops an optimization model to solve the
Time-Constrained Capacitated Vehicle Routing Problem. We share the optimization
code and sample data files
developed at Argonne National Laboratory.

This code and data files are supplementary materials for the manuscript 
"A Time-Constrained Capacitated Vehicle Routing Problem in Urban E-Commerce Delivery"
by Taner Cokyasar, Jeffrey Larson, Monique Stinson, and Olcay Sahin.

Authors and Contributors
--------------------------
Copyright (C) Taner Cokyasar - All Rights Reserved

Unauthorized copying of this file, via any medium is strictly prohibited

Proprietary and confidential

Written by Taner Cokyasar <tcokyasar@anl.gov>, January 2022

System Requirements
-------------------------
Python 3.7.3+

Gurobi 9.1.2+ or Pyomo 6.2+

Running
------------------------
Run main.py in terminal using Python 3.7.3+. Open main.py to adjust instance name, 
time limit, and solver if desired. Default settings are: instance name = 'Chicago_1', 
time limit = 60 seconds, and solver = 'gurobi.'
