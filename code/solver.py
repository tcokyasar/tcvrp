'''The below optimization model solves a time-constrained capacitated vehicle
routing problem. All_loc includes the list of indices of all locations, 
Customer_loc includes only the customer locations as a list, Depot_loc equals
to the depot index, T is a dictionary keyed by a tuple of location to location
and valued as the travel time, Dist is a dictionary keyed by a tuple of 
location to location and valued as the travel distance, S is a dictinary with 
customer_loc keys and service time values, N is the number of units demanded 
at customer locations as a dictionary, T_bar is the upper bound travel time for 
each vehicle, and Q is the capacity limitation for each truck. 
We additionally feed in a time limit named as VRP_optimization_time_limit, 
a possible initial solution as a warm start, and the starting time to account
for the CPU time.'''
def TCVRP_solver_gurobi(All_loc, Customer_loc, Depot_loc, T, Dist, S, D,
                         N, T_bar, Q, VRP_optimization_time_limit, initial, start):
    from gurobipy import Model, quicksum, GRB
    from datetime import datetime
    ### BEGIN CREATING THE MODEL ###
    m = Model("TCVRP")
    timenow = datetime.now()
    ### END CREATING THE MODEL ###

    ### BEGIN INTRODUCING VARIABLES ###
    print("Began creating variables.\n")
    x = {}; y = {}; z = {}
    for i in All_loc:
        for j in All_loc:
            if i != j:
                x[i,j] = m.addVar(vtype=GRB.BINARY, name="x%s"%str([i,j]))
                y[i,j] = m.addVar(vtype=GRB.CONTINUOUS, name="y%s"%str([i,j]))
                z[i,j] = m.addVar(vtype=GRB.CONTINUOUS, name="z%s"%str([i,j]))
    k = m.addVar(vtype=GRB.INTEGER, name = "k")
    print("Time to create variables is %s."%((datetime.now()-timenow).total_seconds()))
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())
    ### END INTRODUCING VARIABLES ###

    ### BEGIN CREATING THE OBJECTIVE FUNCTION ###
    timenow = datetime.now()
    m.setObjective(quicksum(Dist[i,j]*x[i,j] for (i,j) in x.keys())
                    +quicksum(D[i]*x[i,j] for (i,j) in x.keys() if i!=Depot_loc) , GRB.MINIMIZE)
    print('Objective function has been defined.')
    print("Time to write the objective function is %s."%((datetime.now()-timenow).total_seconds()))
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())
    ### END CREATING THE OBJECTIVE FUNCTION ###

    ### BEGIN CREATING THE CONSTRAINTS ###
    m.addConstrs((quicksum(x[i,j] for i in All_loc if i != j) == 1 
                  for j in Customer_loc), name="come_from_one_i")
    m.addConstrs((quicksum(x[i,j] for j in All_loc if i != j) == 1 
                  for i in Customer_loc), name="go_to_a_j")
    m.addConstr((quicksum(x[Depot_loc,i] for i in Customer_loc) == k), 
                name="start_from_depot")
    m.addConstr((quicksum(x[i,Depot_loc] for i in Customer_loc) == k), 
                name="end_at_depot")
    m.addConstrs((quicksum(z[i,j] for j in All_loc if i != j)
                 - quicksum(z[j,i] for j in All_loc if i != j)
                 - quicksum((T[i,j]+S[i])*x[i,j] for j in All_loc if i != j) == 0 
                  for i in Customer_loc),
                name = "time_balance")
    m.addConstrs((z[i,j] <= (T_bar-T[j,Depot_loc])*x[i,j] for (i,j) in x.keys() 
                  if j != Depot_loc), name='Time_upper_bound')
    m.addConstrs((z[i,Depot_loc] <= T_bar*x[i,Depot_loc] for i in Customer_loc),
                 name='Time_upper_bound_to_Depot')
    m.addConstrs((z[i,j] >= (T[i,j]+T[Depot_loc,i]+S[i])*x[i,j] 
                  for (i,j) in x.keys() if i != Depot_loc),
                name = "Time_lower_bound")
    m.addConstrs((z[Depot_loc,i] == T[Depot_loc,i]*x[Depot_loc,i] for i in Customer_loc), 
                 name="Start_time_count_at_depot")
    m.addConstrs((y[i,j] <= Q*x[i,j] for (i,j) in x.keys()), 
                 name="Upper_bound_for_capacity")
    m.addConstrs((quicksum(y[i,j] for j in All_loc if i != j) 
                  == quicksum(y[j,i] for j in All_loc 
                  if i != j)+N[i] for i in Customer_loc), name="Capacity_balance")

    '''Provide a partial initial solution. if you wish.'''
    if len(initial) > 0:
        for i in initial:
            x[i].start = 1
    ### END CREATING THE CONSTRAINTS ###

    ### BEGIN SOLVING ###
    print("Beginning to solve the problem!")
    m.setParam('TimeLimit', VRP_optimization_time_limit)
    m.update(); m.optimize(); print("\n\n")
    print("The problem has been solved.")
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())
    ### END SOLVING ###
    
    if m.status == GRB.INFEASIBLE:
        xvals = 'Infeasible'
    else:
        xvals = [(i,j) for (i,j) in x.keys() if x[i,j].x>0.1]
    
    return(xvals)



'''The below optimization model solves a time-constrained capacitated vehicle
routing problem. All_loc includes the list of indices of all locations, 
Customer_loc includes only the customer locations as a list, Depot_loc equals
to the depot index, T is a dictionary keyed by a tuple of location to location
and valued as the travel time, Dist is a dictionary keyed by a tuple of 
location to location and valued as the travel distance, S is a dictinary with 
customer_loc keys and service time values, N is the number of units demanded 
at customer locations as a dictionary, T_bar is the upper bound travel time for 
each vehicle, and Q is the capacity limitation for each truck. 
We additionally feed in a time limit named as VRP_optimization_time_limit, 
a possible initial solution as a warm start, and the starting time to account
for the CPU time.'''
def TCVRP_solver_pyomo(All_loc, Customer_loc, Depot_loc, T, Dist, S, D, N, T_bar, Q, 
                    VRP_optimization_time_limit, initial, start, solver_name, solver_directory):
    from pyomo.environ import (ConcreteModel, Var, Constraint, SolverFactory, minimize,
                               Binary, NonNegativeReals, PositiveIntegers, Objective)
    from datetime import datetime
    ### BEGIN CREATING THE MODEL ###
    m = ConcreteModel("TCVRP")
    timenow = datetime.now()
    ### END CREATING THE MODEL ###

    ### BEGIN INTRODUCING VARIABLES ###
    print("Began creating variables.")
    m.x = Var(All_loc, All_loc, domain=Binary)
    m.y = Var(All_loc, All_loc, domain=NonNegativeReals)
    m.z = Var(All_loc, All_loc, domain=NonNegativeReals)
    m.k = Var(domain=PositiveIntegers)
    print("Time to create variables is %s."%((datetime.now()-timenow).total_seconds()))
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())
    ### END INTRODUCING VARIABLES ###

    ### BEGIN CREATING THE OBJECTIVE FUNCTION ###
    timenow = datetime.now()
    m.OBJ = Objective(expr = sum(Dist[i,j]*m.x[i,j] for i in All_loc 
                       for j in All_loc if i!=j)
                       + sum(D[i]*m.x[i,j] for i in All_loc 
                       for j in All_loc if i!=j and i!=Depot_loc),
                      sense=minimize)
    print('Objective function has been defined.')
    print("Time to write the objective function is %s."%((datetime.now()-timenow).total_seconds()))
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())
    ### END CREATING THE OBJECTIVE FUNCTION ###
   
    ### BEGIN CREATING THE CONSTRAINTS ###
    timenow = datetime.now()
    print("Starting to create constraints: come_from_one_i.\n")
    def Cons1(m, j):
        return (sum(m.x[i, j] for i in All_loc if i!=j) == 1)
    m.AxbConstraint = Constraint(Customer_loc, rule=Cons1)
    print('\ncome_from_one_i constraints are done.')
    print("Time to write come_from_one_i constraints is %s."%((datetime.now()-timenow).total_seconds()))
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())
    
    timenow = datetime.now()
    print("Starting to create constraints: go_to_a_j.\n")
    def Cons2(m, i):
        return (sum(m.x[i, j] for j in All_loc if i!=j) == 1)
    m.AxbConstraint2 = Constraint(Customer_loc, rule=Cons2)
    print('\ngo_to_a_j constraints are done.')
    print("Time to write go_to_a_j constraints is %s."%((datetime.now()-timenow).total_seconds()))
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())
    
    timenow = datetime.now()
    print("Starting to create constraints: start_from_depot.\n")
    def Cons3(m):
        return (sum(m.x[Depot_loc, i] for i in Customer_loc) == m.k)
    m.AxbConstraint3 = Constraint(rule=Cons3)
    print('\nstart_from_depot constraints are done.')
    print("Time to write start_from_depot constraints is %s."%((datetime.now()-timenow).total_seconds()))
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())
    
    timenow = datetime.now()
    print("Starting to create constraints: end_at_depot.\n")
    def Cons4(m):
        return (sum(m.x[i, Depot_loc] for i in Customer_loc) == m.k)
    m.AxbConstraint4 = Constraint(rule=Cons4)
    print('\nend_at_depot constraints are done.')
    print("Time to write end_at_depot constraints is %s."%((datetime.now()-timenow).total_seconds()))
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())
    
    timenow = datetime.now()
    print("Starting to create constraints: time_balance.\n")
    def Cons5(m, i):
        return (sum(m.z[i,j] for j in All_loc if i != j)
                 - sum(m.z[j,i] for j in All_loc if i != j)
                 - sum((T[i,j]+S[i])*m.x[i,j] for j in All_loc if i != j) == 0)
    m.AxbConstraint5= Constraint(Customer_loc, rule=Cons5)
    print('\ntime_balance constraints are done.')
    print("Time to write time_balance constraints is %s."%((datetime.now()-timenow).total_seconds()))
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())
    
    timenow = datetime.now()
    print("Starting to create constraints: Time_upper_bound.\n")
    def Cons6(m, i, j):
        if i != j and j != Depot_loc:
            return (m.z[i,j] <= (T_bar-T[j,Depot_loc])*m.x[i,j])
        else:
            return Constraint.Skip
    m.AxbConstraint6= Constraint(All_loc, All_loc, rule=Cons6)
    print('\nTime_upper_bound constraints are done.')
    print("Time to write Time_upper_bound constraints is %s."%((datetime.now()-timenow).total_seconds()))
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())
   
    timenow = datetime.now()
    print("Starting to create constraints: Time_upper_bound_to_Depot.\n")
    def Cons7(m, i):
        return (m.z[i,Depot_loc] <= T_bar*m.x[i,Depot_loc])
    m.AxbConstraint7= Constraint(Customer_loc, rule=Cons7)
    print('\nTime_upper_bound_to_Depot constraints are done.')
    print("Time to write Time_upper_bound_to_Depot constraints is %s."
          %((datetime.now()-timenow).total_seconds()))
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())
    
    timenow = datetime.now()
    print("Starting to create constraints: Time_lower_bound.\n")
    def Cons8(m, i, j):
        if i != j and i != Depot_loc:
            return (m.z[i,j] >= (T[i,j]+T[Depot_loc,i]+S[i])*m.x[i,j])
        else:
            return Constraint.Skip
    m.AxbConstraint8= Constraint(All_loc, All_loc, rule=Cons8)
    print('\nTime_lower_bound constraints are done.')
    print("Time to write Time_lower_bound constraints is %s."
          %((datetime.now()-timenow).total_seconds()))
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())
    
    timenow = datetime.now()
    print("Starting to create constraints: Start_time_count_at_depot.\n")
    def Cons9(m, i):
        return (m.z[Depot_loc,i] == T[Depot_loc,i]*m.x[Depot_loc,i])
    m.AxbConstraint9= Constraint(Customer_loc, rule=Cons9)
    print('\nStart_time_count_at_depot constraints are done.')
    print("Time to write Start_time_count_at_depot constraints is %s."
          %((datetime.now()-timenow).total_seconds()))
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())
    
    timenow = datetime.now()
    print("Starting to create constraints: Upper_bound_for_capacity.\n")
    def Cons10(m, i, j):
        if i != j:
            return (m.y[i,j] <= Q*m.x[i,j])
        else:
            return Constraint.Skip
    m.AxbConstraint10= Constraint(All_loc, All_loc, rule=Cons10)
    print('\nUpper_bound_for_capacity constraints are done.')
    print("Time to write Upper_bound_for_capacity constraints is %s."
          %((datetime.now()-timenow).total_seconds()))
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())
    
    timenow = datetime.now()
    print("Starting to create constraints: Capacity_balance.\n")
    def Cons11(m, i):
        return (sum(m.y[i,j] for j in All_loc if i != j) 
                  == sum(m.y[j,i] for j in All_loc 
                  if i != j)+N[i])
    m.AxbConstraint11= Constraint(Customer_loc, rule=Cons11)
    print('\nCapacity_balance constraints are done.')
    print("Time to write Capacity_balance constraints is %s."
          %((datetime.now()-timenow).total_seconds()))
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())

    '''Provide a partial initial solution.'''
    if len(initial) > 0:
        for i in initial:
            m.x[i].value = 1
    ### END CREATING THE CONSTRAINTS ###
    
    ### BEGIN SOLVING ###
    print("Beginning to solve the problem!\n")
    if solver_directory != 'N/A':
        opt = SolverFactory('{}'.format(solver_name), 
                            executable="{}".format(solver_directory))
    else:
        opt = SolverFactory('{}'.format(solver_name))
    
    results = opt.solve(m, tee=True, timelimit=VRP_optimization_time_limit)
    print("The problem has been solved.")
    print("Current elapsed time is %s seconds.\n"%(datetime.now()-start).total_seconds())
    ### END SOLVING ###
    
    xvals = [(i,j) for i in All_loc for j in All_loc if i != j and m.x[i,j]()>0.1]
    
    return(xvals)