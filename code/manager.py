'''This file contains a function that routes the problem instance to a desired linear (or possiblly 
nonlinear) programming solver. If Gurobi is used as a solver, the instance will be routed to 
TCVRP_solver_gurobi function in the solver.py file, and Gurobi's Python programming syntax is used
to construct and solve the instance. Otherwise, the instance will be routed to TCVRP_solver_pyomo
function in the solver.py file, and Pyomo syntax is used to construct and solve the instance with 
the specified solver (requires installation) and its directory.'''
def TCVRP_solver_main(All_loc, Customer_loc, Depot_loc, T, Dist, S, P, D, U, Q, 
                    VRP_optimization_time_limit, initial, start, solver_name, solver_directory):
    ####################################################################################################
    ###################################### Importing solver packages ###################################
    ####################################################################################################
    acceptable_solvers = ["gurobi", "cplex", "glpk", "cbc", "ipopt", "knitroampl"]
    if solver_name == "gurobi":
        from solver import TCVRP_solver_gurobi
    else:
        if solver_name in acceptable_solvers:
            from solver import TCVRP_solver_pyomo
        else:
            raise NameError('''Please input an acceptable solver_name in ProgramControl.csv.
                            Acceptable solver names are: gurobi, cplex, glpk, cbc, ipopt, knitroampl.
                            Make sure the solver is installed on your computer, and Pyomo can call it.
                            ''')

    ####################################################################################################
    ################################### Solving the problem instance ###################################
    ####################################################################################################
    print("%s is being used as the solver.\n"%(solver_name))
    if solver_name == "gurobi":
        return TCVRP_solver_gurobi(All_loc, Customer_loc, Depot_loc, T, Dist, S, 
                             P, D, U, Q, VRP_optimization_time_limit, initial, 
                             start)
    else:
        return TCVRP_solver_pyomo(All_loc, Customer_loc, Depot_loc, T, Dist, S, 
                             P, D, U, Q, VRP_optimization_time_limit, initial, 
                             start, solver_name, solver_directory)