import csv, os
from datetime import datetime
from manager import TCVRP_solver_main

Instance_name = 'Chicago_1' # Input instance name
time_limit = 60 #seconds
solver_name = 'gurobi' #See available solvers in main.py
solver_directory = 'N/A' #Fill 'N/A' if not available or added to path

TT_data_path = ('../data/'+Instance_name+'_TT.csv')
Dist_data_path = ('../data/'+Instance_name+'_Dist.csv')
Info_data_path = ('../data/'+Instance_name+'_Info.csv')
Input_data_path = ('../data/'+Instance_name+'_Input.csv')

print('Reading %s.\n'%(TT_data_path))
with open(TT_data_path, mode ='r') as file:
    Travel_time = [l for l in csv.reader(file)]
print('Finished reading %s.\n'%(TT_data_path))

print("Constructing the TT dictionary.\n")
TT = {}
for i in range(len(Travel_time[0][1:])):
    for j in range(len(Travel_time[0][1:])):
        TT[int(Travel_time[0][1:][i]), int(Travel_time[0][1:][j])] = float(Travel_time[i+1][1:][j])
print("Finished constructing the TT dictionary.\n")

print('Reading %s.\n'%(Dist_data_path))
with open(Dist_data_path, mode ='r') as file:
    Travel_dist = [l for l in csv.reader(file)]
print('Finished reading %s.\n'%(Dist_data_path))

print("Constructing the Dist dictionary.\n")
Dist = {}
for i in range(len(Travel_dist[0][1:])):
    for j in range(len(Travel_dist[0][1:])):
        Dist[int(Travel_dist[0][1:][i]), int(Travel_dist[0][1:][j])] = float(Travel_dist[i+1][1:][j])
print("Finished constructing the Dist dictionary.\n")

print('Reading %s.\n'%(Info_data_path))
with open(Info_data_path, mode ='r') as file:
    Rows = [l for l in csv.reader(file)][1:]
print('Finished reading %s.\n'%(Info_data_path))

print("Constructing the required dictionaries.\n")
Num_cust = {int(i[0]):int(i[1]) for i in Rows}
Serv_time = {int(i[0]):float(i[2]) for i in Rows}
Serv_dist = {int(i[0]):float(i[3]) for i in Rows}
print("Finished constructing the required dictionaries.\n")

print('Reading %s.\n'%(Input_data_path))
with open(Input_data_path, mode ='r') as file:
    Gen = [l for l in csv.reader(file)]
print('Finished reading %s.\n'%(Input_data_path))

Num_packs = int(Gen[0][0])
Op_seconds = int(Gen[1][0])*3600 #Converting hours to seconds

Initial_solution = [] #Provide an initial solution if have one

Points = list(set(i[0] for i in TT.keys()))
Points_except_depot = list(Num_cust.keys())
Depot = list(set(Points) - set(Points_except_depot))[0]

start = datetime.now()
xvals = TCVRP_solver_main(Points, Points_except_depot, Depot, TT, Dist, Serv_time,
                          Serv_dist, Num_cust, Op_seconds, Num_packs, time_limit, 
                          Initial_solution, start, solver_name, solver_directory)
'''xvals provide the results.'''